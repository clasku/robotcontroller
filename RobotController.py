"""
Toy Robot Simulator
--------------------
Create an application that can read in commands of the following form:

PLACE X,Y,F
MOVE
LEFT
RIGHT
REPORT

. The application is a simulation of a toy robot moving on a square tabletop, of dimensions 5 units x 5 units.
. There are no obstructions on the table surface.
. The robot is free to roam around the surface of the table, but must be prevented from falling to destruction. Any movement that would result in the robot falling from the table must be prevented, however further valid movement commands must still be allowed.
. PLACE will put the toy robot on the table in position X,Y and facing NORTH, SOUTH, EAST or WEST.
. The origin (0,0) can be considered to be the SOUTH WEST most corner.
. The first valid command to the robot is a PLACE command, after that, any sequence of commands may be issued, in any order, including another PLACE command. The application should discard all commands in the sequence until a valid PLACE command has been executed.
. MOVE will move the toy robot one unit forward in the direction it is currently facing.
. LEFT and RIGHT will rotate the robot 90 degrees in the specified direction without changing the position of the robot.
. REPORT will announce the X,Y and orientation of the robot.
. A robot that is not on the table can choose to ignore the MOVE, LEFT, RIGHT and REPORT commands.
. The robot must not fall off the table during movement. This also includes the initial placement of the toy robot. Any move that would cause the robot to fall must be ignored.

Example Input and Output:
a)
PLACE 0,0,NORTH
MOVE
REPORT
Output: 0,1,NORTH

b)
PLACE 0,0,NORTH
LEFT
REPORT
Output: 0,0,WEST

c)
PLACE 1,2,EAST
MOVE
MOVE
LEFT
MOVE
REPORT
Output: 3,3,NORTH

- There must be a way to supply the application with input data.
- Provide test data to exercise the application.
- The application must run and you should provide sufficient evidence that your solution is complete by, as a minimum, indicating that it works correctly against the supplied test data.
- The submission should be production quality and it can be done in any language (using JavaScript, Ruby or Go would be a bonus).
- The code should be original and you may not use any external libraries or open source code to solve this problem, but you may use external libraries or tools for building or testing purposes. Specifically, you may use unit testing libraries or build tools available for your chosen language.
"""

class RobotController:
    """ Define the Robot Controller
    """

    cardinal_points = [["NORTH", (0, 1)], ["EAST", (1, 0)], ["SOUTH", (0, -1)], ["WEST", (-1, 0)]]

    def __init__(self, x_len, y_len, track=True):
        """ x_len = x axis lenght
            y_len = y axis lenght
        """
        self.x_values = [x for x in range(0, x_len)]
        self.y_values = [y for y in range(0, y_len)]
        self.status = False
        if track:
            self.track = []
        else:
            self.track = track
        self.cp_names = [c[0] for c in self.cardinal_points]
        self.cp_moves = [c[1] for c in self.cardinal_points]
        return

    def stay_on_table(self, value, axis):
        """ check if the robot is on the table
                value = value to check
                axis = axis for checking (0, 1)
        """
        if axis == 0:
            return value in self.x_values
        if axis == 1:
            return value in self.y_values

    def move(self):
        """ move the robot one unit forward to the facing direction
        """
        if not self.robot_is_on_table():
            return False

        f_index = self.cp_names.index(self.f)
        new_x = self.x + self.cp_moves[f_index][0]
        new_y = self.y + self.cp_moves[f_index][1]

        if self.stay_on_table(new_x, 0) and self.stay_on_table(new_y, 1):
            self.x = new_x
            self.y = new_y
            self.tracker()
            return True 
        else:
            return False

    def place(self, x, y, f):
        """ place the robot on the table
                x = x position coordinate 
                y = y position coordinate
                f = facing cardinal point (NORTH, SOUTH, EAST, WEST)
        """
        if x  not in self.x_values or y not in self.y_values:
            return False
        if f.upper() not in self.cp_names:
            return False
        self.x = x 
        self.y = y 
        self.f = f
        self.status = True
        self.tracker()
        return True

    def left(self):
        """ rotate robot to the left
        """
        if not self.robot_is_on_table():
            return False

        f_index = self.cp_names.index(self.f)
        new_f_value = f_index - 1
        if new_f_value < 0:
            self.f = self.cp_names[len(self.cp_names) - 1]
            return True
        self.f = self.cp_names[new_f_value]
        return True

    def right(self):
        """ rotate robot to the right
        """
        if not self.robot_is_on_table():
            return False

        f_index = self.cp_names.index(self.f)
        new_f_value = f_index + 1

        if new_f_value >= len(self.cp_names):
            self.f = self.cp_names[0]
            return True
        self.f = self.cp_names[new_f_value]
        return True

    def report(self):
        """ announce position and orientation of the robot
        """
        if not self.robot_is_on_table():
            print("There is not a robot on the table")
            return False
        print("X position: {}\nY position: {}\nFacing: {}".format(self.x, self.y, self.f))
        return True

    def robot_is_on_table(self):
        """ check if a robot is on the table 
        """
        return self.status

    def tracker(self):
        """ track all robot moves
        """
        if self.track == False:
            return False
        self.track.append((self.x, self.y))
        return True

if __name__ == '__main__':
    my_robot = RobotController(5, 5, track=False)

    ##### TESTING
    """Example Input and Output:
    a)
    PLACE 0,0,NORTH
    MOVE
    REPORT
    Output: 0,1,NORTH
    """
    print("test a)")
    my_robot.place(0, 0, "NORTH")
    my_robot.move()
    my_robot.report()
    print("-" * 20)

    """b)
    PLACE 0,0,NORTH
    LEFT
    REPORT
    Output: 0,0,WEST
    """
    print("test b)")
    my_robot.place(0, 0, "NORTH")
    my_robot.left()
    my_robot.report()
    print("-" * 20)

    """c)
    PLACE 1,2,EAST
    MOVE
    MOVE
    LEFT
    MOVE
    REPORT
    Output: 3,3,NORTH
    """
    print("test c)")
    my_robot.place(1, 2, "EAST")
    my_robot.move()
    my_robot.move()
    my_robot.left()
    my_robot.move()
    my_robot.report()
    print("-" * 20)
    print(my_robot.track)